# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [6.2.0] - 2024-11-04

### Changed

- Update to search v1.2.0

## [6.1.0] - 2024-10-07

### Changed

- Update to search v1.1.0

## [6.0.1] - 2024-10-04

### Changed

- Update to search v1.0.1

## [6.0.0] - 2024-10-04

### Added

- Custom vue search app css and js

### Removed

- Search form css and js

## [5.1.0] - 2024-09-12

### Added

- og:site_name facet for search form

## [5.0.2] - 2024-08-20

### Added

- npm build script

### Removed

- Blocking unused scss

## [5.0.1] - 2024-08-19

### Changed

- Search form action to point to search.tcu.edu instead of dev

## [5.0.0] - 2024-08-19

### Added

- Search form that submits to search.tcu.edu

### Removed

- References to Sajari

## [4.1.0] - 2024-07-29

### Added

- body styles to purgeCSS whitelist

### Removed

- _buttons.scss partial
- _navigation.scss module

## [4.0.0] - 2024-07-29

### Changed

- File paths for alertcheck and errorlog

### Removed

- TCU Navigation burger nav icon
- TCU Navigation panel
_ TCU Navigation JS

## [3.11.0] - 2024-03-04

### Changed

- Parents & Family link

## [3.10.0] - 2023-12-28

### Added

- ARIA label to header section

### Changed

- header div to section

## [3.9.0] - 2023-12-18

### Added

- Added color to visually hidden span text in closing button for accessibility

### Changed

- tag for global header to div

## [3.8.0] - 2023-07-20

### Changed

- Link to Accreditation in Footer

## [3.7.0] - 2023-07-10

### Added

- title attribute to <header>

## [3.6.0] - 2023-06-30

### Changed

- Visibility of Search label for mobile & tablet

## [3.5.0] - 2023-04-10

### Changed

- Media link in nav

## [3.4.0] - 2023-01-27

### Changed

- Remove <title> from logo SVGs

## [3.3.0] - 2022-01-31

### Added

- hide_search_script global

## [3.2.2] - 2022-01-14

### Changed

- Copyright dynamically displays current year

## [3.2.1] - 2021-12-17

### Added

- Dart Sass (node v15+ compatibility)

### Changed

- "Work at TCU" footer link

### Removed

- node-sass
