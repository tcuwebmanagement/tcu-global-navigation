<style>
<!-- inject:../../site/_resources/css/style.min.css -->
<!-- endinject -->
</style>

<!-- begin TCU global nav -->
<section aria-label="TCU global header" class="tcu-global-header__wrapper">
    <div class="tcu-global-header tcu-layout-constrain cf">
        <div class="tcu-global-header-left">
            <div class="tcu-global-logo-wrap">
                <a class="tcu-global-logo" href="https://www.tcu.edu/" aria-label="Open global TCU Navigation menu" rel="nofollow">
										<svg xmlns="http://www.w3.org/2000/svg" height="51" width="105" viewBox="0 0 357.9 172.9"><path fill="#4D1979" d="M335.1 149.8c3.1 0 5.7 2.5 5.7 5.7s-2.6 5.7-5.7 5.7-5.7-2.5-5.7-5.7 2.6-5.7 5.7-5.7zm0 12.6c3.8 0 6.9-3 6.9-6.8s-3-6.8-6.9-6.8c-3.8 0-6.9 3-6.9 6.8.1 3.7 3.1 6.8 6.9 6.8zm1.2-6.5c1 0 2.1-.6 2.1-2.1 0-1.6-1.3-2-2.6-2h-3.2v7.7h1.1V156h1.4l2 3.5h1.3l-2.1-3.6zm-.8-3.1c.9 0 1.7.1 1.7 1 0 1-1.4.9-2.5.9h-1.2v-1.9h2zM108.2 3.8C51.3 11.4 5.7 31.5 3.8 32.3l-1 .5L0 34v60l6.7-3.2 2.3-1c2.9-1.4 10.9-5 22.7-9.6l1-.4 3.1-1.1V59c1.8-.6 3.5-1.2 5.1-1.8v68.4c-4 1.6-7.7 3.2-11.2 4.7l-1 .4-2.8 1.2v40.9l6.8-3.3 2.3-1.1c16.9-8.1 34.3-15.1 51.8-20.7l1.1-.3 3.4-1v-37.9l-6 1.8-2.1.6c-2.3.7-4.7 1.4-7 2.1V46.6c1.9-.5 3.7-.9 5.4-1.3v19.3l5.8-1.3 2-.5c7.3-1.7 14.2-3 20.6-4.1l1.4-.2 3.9-.7V2.9l-5.3.7-1.8.2zm92.1-1.3L199 1.2l-1.8-.2h-.5c-6.6-.7-13.8-1-21.6-1-11 0-19.6.7-21.4.8h-.5l-1.9.2-1.3 1.3-26.1 26.9-1.3 1.4v78.1l1.3 1.4 24.6 26.1 1.6 1.7 2.3-.2.8-.1c1-.1 9.9-.8 21.5-.8 7.7 0 14.8.3 21 .9l.7.1 2.3.3 1.6-1.7 25.7-26.3 1.3-1.4V81.4L223 81l-25.9-3-5.2-.6v15.8c-1.9 1.9-5.3 5.3-7.3 7.2-3.1-.1-6.3-.2-9.6-.2-4.3 0-8.2.1-11.1.2-1.6-1.7-4.1-4.3-5.6-5.9V45.3c.8-.8 1.9-2 3.1-3.2 2-2.1 4.2-4.4 5.6-5.8 2.7-.1 5.4-.1 8-.1 2.6 0 5.2 0 7.6.1 2.3 2.2 6.8 6.7 9.1 9v13.4l4.2.4 25.9 2.6 5.2.5V30.6l-1.3-1.4-25.4-26.7zm154.9 35l-.9-.4c-12.9-6.4-28.4-12.4-45.8-17.6l-2.2-.6-6-1.7v37.9l3.2 1.1c1.8.6 4.2 1.4 6.1 2v60.1c-1.8 1.6-4.8 4.1-6.8 5.9-4.1-1.5-11.6-4.4-14.9-5.6-1.8-3-6-9.9-7.6-12.6V49.3c2.8.7 5.6 1.4 5.8 1.5l5.9 1.5V14.7l-3.7-.8-1.2-.3c-21.6-5.4-39.6-8.2-46.3-9.1l-1.8-.3-5.4-.8v36.9l4.1.6 1.4.2h.1c.8.1 2.4.3 4.6.7v72.8l.7 1.1L264 148l1 1.6 1.8.5.6.2c3.1.9 19.8 5.8 37 14.4l1 .5 2.8 1.4 2.4-2.1 32.9-28.4 1.6-1.4V72.5l3.6 1.8 2.4 1.2 6.8 3.4V38.7l-2.7-1.2z"/></svg>
                    <span class="tcu-visuallyhidden">Texas Christian University</span>
                </a>
            </div>
        </div><!--/ .header-left -->

        <div class="tcu-global-header-right">
            <?php if (!isset($GLOBALS["hide_search"]) || $GLOBALS["hide_search"] != true) { ?>
            <button class="search" data-tcu-search data-type="modal" data-site-scoping="true">
                <span class="icon-label">Search</span><svg focusable="false" height="16" width="16" class="search-icon" viewBox="0 0 27 31"><path d="M3.552 10c0-3.564 2.917-6.468 6.503-6.468 3.587 0 6.504 2.904 6.504 6.468 0 3.568-2.917 6.47-6.504 6.47-3.586 0-6.503-2.902-6.503-6.47m23.115 17.586l-7.555-10.008h-.546l-.548.545-.806-1.107A9.934 9.934 0 0020.11 10c0-5.513-4.51-10-10.056-10C4.511 0 0 4.487 0 10c0 5.516 4.51 10.002 10.055 10.002 1.643 0 3.191-.403 4.563-1.1l1.006 1.141-.605.604v.286s7.439 9.29 7.9 9.749c.461.459 1.499.286 1.499.286s1.5-.86 2.25-1.605c.748-.746 0-1.777 0-1.777" fill-rule="evenodd"/></svg>
            </button>
            <?php } ?>
        </div>
    </div><!-- / .global-header -->
</section><!-- / .tcu-global-header__wrapper -->


<?php if (!isset($GLOBALS["hide_search_script"]) || $GLOBALS["hide_search_script"] != true) { ?>
<script type="module" crossorigin src="https://assets.tcu.edu/js/search/v1.2.0/main.js"></script>
<?php } ?>
<!-- end of global TCU nav include -->
<?php include_once __DIR__ . '/_alerts.php'; ?>
