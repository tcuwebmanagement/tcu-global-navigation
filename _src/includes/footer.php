<footer class="tcu-footer cf">
    <?php include 'global-footer.php'; ?>

    <!-- Scroll to top -->
    <button type="button" class="tcu-top" title="Back to top"><span class="tcu-visuallyhidden">Top</span></button>
</footer>
