<style>
<!-- inject:../../site/_resources/css/footer.min.css -->
<!-- endinject -->
</style>

<div class="tcu-footer-nav-wrap tcu-layoutwrap--purple tcu-pad-tb0 cf">
	<nav aria-label="Complementary">
		<ul class="tcu-footer-nav cf">
			<li><a href="https://hr.tcu.edu/prospective-employees/index.php">Work at TCU</a></li>
			<li><a href="https://accessibility.tcu.edu">Accessibility</a></li>
			<li><a href="https://www.tcu.edu/compliance/notice-of-nondiscrimination.php">Notice of Nondiscrimination</a></li>
			<li><a href="https://www.tcu.edu/institutional-equity/title-ix/index.php">Title IX</a></li>
			<li><a href="https://www.tcu.edu/compliance/legal-disclosures.php">Legal Disclosures</a></li>
			<li><a href="https://www.tcu.edu/compliance/privacy.php">Privacy</a></li>
			<li><a href="https://ie.tcu.edu/accreditation/">Accreditation</a></li>
		</ul>
	</nav>
	<div class="tcu-copyright">
		<p>All content <span id="directedit">&copy;</span> <?php echo date('Y');?> <a href="https://www.tcu.edu">Texas Christian University</a>. All rights reserved.</p>
	</div>
</div>
