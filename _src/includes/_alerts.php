<!-- TCU Alerts (PRODUCTION) -->
<?php
/**
 * Polling and display logic for TCU Emergency Alerts
 *
 * @package tcuedu_first
 * @author Corey Reed <corey.reed@tcu.edu>
 * ASP version by Ben Crenshaw <b.e.crenshaw@tcu.edu>
 * JavaScript by Mayra Perales <m.j.perales@tcu.edu>
 * @since 0.1 (beta development)
 */

$active_alert  = false;
$cache_seconds = 30;
$timezone      = new DateTimeZone( 'UTC' );
$now           = new DateTime( 'NOW', $timezone );
$next_update   = new DateTime( 'NOW', $timezone );
$next_update->modify( '+' . $cache_seconds . ' seconds' );

$alert_file    = __DIR__ . '/_alertcheck.txt';
$alert_message = '';

// Testing endpoint: https://alertsvctest.tcu.edu/api/alerts/getcurrentgroup
// two endpoints are available: GetCurrentAlert and GetCurrentGroup.
// GetCurrentAlert will only return the most recent message from Everbridge.
$alert_service_url = 'https://alertservice.tcu.edu/api/alerts/GetCurrentGroup';
//$alert_service_url = 'https://alertsvctest.tcu.edu/api/alerts/getcurrentgroup';

$max_items = 5;

// error handling options.
$email_delay_minutes = 15;
$error_recipient     = 'corey.reed@tcu.edu';
$error_file          = __DIR__ . '/_errorlog.txt';
$last_email          = file_exists( $error_file ) ? file_get_contents( $error_file ) : false;
$error_message       = '';

// Work your magic.
_init( $alert_file );

/**
 * Check for and display any active alert(s).
 *
 * @param string $alert_file The filepath for the alert cache file.
 * @param bool $active_alert The flag for presence of an active alert.
 * @param object $alert_message The collection of alert messages to display.
 */
function _init( $alert_file ) {
	$active_alert  = false;
	$alert_message = '';

	if ( false === file_check( $alert_file ) ) {
		$alert_message = fetch_messages();
	} else {
		$alert_message = $GLOBALS['alert_message'];
	}

	$active_alert = $GLOBALS['active_alert'];

	// if message contains an alert, build out the HTML
	// and display it.
	if ( true === $active_alert ) {
		echo build_alert( $alert_message );
	}
}

/**
 * Test for presence of alert cache file.
 * If file exists, check the timestamp for freshness.
 * If cache has not expired, test for critical components (timestamp and message).
 *
 * @param string $file The filepath for the file to check.
 * @return bool $is_valid
 */
function file_check( $file ) {
	// @return Boolean.
	$is_valid = false;

	// check for cached message file: _alertcheck.txt.
	if ( true === file_exists( $file ) ) {
		$alert_file = json_decode( file_get_contents( $file ) );

		// check for an empty or broken file.
		if ( null === $alert_file || false === $alert_file || ! isset( $alert_file->{'timestamp'} ) || ! isset( $alert_file->{'message'} ) ) {
			$is_valid = false;
			return $is_valid;
		}

		// grab the timestamp from the cache file.
		$timestamp = $alert_file->{'timestamp'};

		// convert Unix timestamp into DateTime object.
		$file_next_update = DateTime::createFromFormat( 'U', $timestamp, $GLOBALS['timezone'] );

		if ( $file_next_update < $GLOBALS['now'] ) {
			// cached file is expired.
			$is_valid = false;
		} else {
			// check for active alert within message.
			check_message( $alert_file->{'message'} );

			// update global alert_message with message from file.
			$GLOBALS['alert_message'] = $alert_file->{'message'};
			$is_valid                 = true;
		}
	}

	return $is_valid;
}

/**
 * Fetch messages from TCU Alert endpoint.
 *
 * @return mixed $alert_messages contains block of alerts in JSON format.
 */
function fetch_messages() {
	$alert_messages = get_request( $GLOBALS['alert_service_url'] );

	file_put_contents(
		$GLOBALS['alert_file'],
		json_encode(
			array(
				'timestamp'  => $GLOBALS['next_update']->getTimestamp(),
				'message'    => $alert_messages,
				'last_email' => $GLOBALS['last_email'],
			)
		)
	);
	check_message( $alert_messages );

	return $alert_messages;
}

/**
 * Check message block for essential components.
 * Sets the global $active_alert flag.
 *
 * @param object $message The block of messages in JSON format.
 */
function check_message( $message ) {
	$active_alert = false;

	// decode the block of message data.
	$message_block = json_decode( $message );

	if ( isset( $message_block[0]->{'Error'} ) ) {
		handle_error( $message_block[0]->{'Error'} );
	} elseif ( ! isset( $message_block[0]->{'Message'} ) ) {
		handle_error( 'Message is missing in the data.' );
	} elseif ( 0 < strlen( $message_block[0]->{'Message'} ) ) {
		$active_alert = true;
	}

	$GLOBALS['active_alert'] = $active_alert;
}

/**
 * Format the HTML of the alert for on-page display.
 *
 * @param object $messages JSON object containing 1 or more messages.
 * @return mixed
 */
function build_alert( $messages ) {
	$message_input  = json_decode( $messages );
	$message_block  = '';
	$date_format    = 'Y-m-d\TH:i:s';
	$i              = 0;
	$max_num_alerts = $GLOBALS['max_items'];

	// loop over each message in the block.
	foreach ( $message_input as $alert ) {
		// limit alert items to the global $max_items.
		if ( $max_num_alerts > $i ) {
			// reformat the date.
			$alert_time     = DateTime::createFromFormat( $date_format, $alert->{'Time'}, $GLOBALS['timezone'] );
			$message_block .= '<div class="tcu-message"><span class="time-stamp"><em>' . $alert_time->format( 'm/d/y \a\t g:i A' ) . '</em></span><p class="tcu-message-notification">' . $alert->{'Message'} . '</p></div>';
		} else {
			break;
		}

		$i++;
	}

  $message_header = '<div class="tcu-alert _emergency-alert" role="alertdialog" aria-labelledby="alertHeading" aria-describedby="alertText"><div class="container"><button id="notification-toggle" data-toggle="open"><span class="tcu-visuallyhidden">Close Alert</span><div class="close" data-toggle="visible"><svg viewBox="0 0 40 40"><path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30"></path></svg></div></button>';
  $message_body   = '<h2 id="alertHeading" class="h2 tcu-message-title">TCU ALERT</h2><span id="alertText">' . $message_block . '</span>';
  $message_footer = '<div class="tcu-message-button-wrapper"><a href="https://www.tcu.edu/news/index.php" class="tcu-button _ou-alert">Full Details</a></div><span class="tcu-icon-container"><img src="https://www.tcu.edu/_resources/library/images/emergency-icon.svg" class="tcu-icon" alt=""></span></div></div>';

	$message = $message_header . $message_body . $message_footer;

	return $message;
}

/**
 * Error handling logic for logging and email notification.
 *
 * @param string $error_message The description of the error.
 * @param string $type The categorical type of error.
 */
function handle_error( $error_message, $type = 'unknown' ) {
	$error_time = $GLOBALS['now'];
	$last_email = DateTime::createFromFormat( 'U', $GLOBALS['last_email'], $GLOBALS['timezone'] );
	$timeout    = $GLOBALS['email_delay_minutes'];
	$expiration = $last_email ? $last_email->modify( '+' . $timeout . ' minutes' ) : false;
	$log_type   = 0; // 0 = log only; 1 = log & email

	// build email notification.
	$notification  = 'An error occurred when running the TCU Alert script (PHP)' . "\r\n\r\n";
	$notification .= 'Time: ' . $error_time->format( 'm/d/Y H:i:s' ) . "\r\n";
	$notification .= 'Error Type: ' . $type . "\r\n";
	$notification .= 'Error Description: ' . $error_message;
	$message       = substr( wordwrap( $notification, 70, "\r\n" ), 0, 250 );

	if ( false === $last_email || $error_time > $expiration ) {
		// no recent email, send error to log and email.
		file_put_contents( $GLOBALS['error_file'], $GLOBALS['now']->getTimestamp() );
		error_log( $message, 1, $GLOBALS['error_recipient'] );
	} else {
		error_log( $message, 0 );
	}

	exit;
}

/**
 * Curl send get request, support HTTPS protocol
 *
 * @param string $url The request URL.
 * @param int    $timeout The timeout in seconds.
 * @return mixed
 */
function get_request( $url, $timeout = 10 ) {
	$ssl      = stripos( $url, 'https://' ) === 0 ? true : false;
	$curl_obj = curl_init();
	$headers  = array(
		'Accept: application/json',
		'Expect: ',
	);
	$options  = [
		CURLOPT_URL            => $url,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_FAILONERROR    => 1,
		CURLOPT_FOLLOWLOCATION => 1,
		CURLOPT_AUTOREFERER    => 1,
		CURLOPT_USERAGENT      => 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)',
		CURLOPT_TIMEOUT        => $timeout,
		CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_0,
		CURLOPT_HTTPHEADER     => $headers,
		CURLOPT_IPRESOLVE      => CURL_IPRESOLVE_V4,
	];
	if ( $ssl ) {
		// support https.
		$options[ CURLOPT_SSL_VERIFYHOST ] = false;
		$options[ CURLOPT_SSL_VERIFYPEER ] = false;
	}
	curl_setopt_array( $curl_obj, $options );
	$return_data = curl_exec( $curl_obj );
	if ( curl_errno( $curl_obj ) ) {
		// handle any URL request errors.
		handle_error( curl_error( $curl_obj ), 'get_request' );
	}
	curl_close( $curl_obj );

	return $return_data;
}
?>

<?php
if ( $active_alert ) {
	?>
  <script>
  (function(){
      var alertWrap = document.querySelector('.tcu-alert._emergency-alert');
      var alertLink = alertWrap.querySelector('#notification-toggle');
      var toggleButton = document.getElementById('notification-toggle');
      var localState = localStorage.getItem('tcu_alert') || 'open';
      var firstFocusableBody = document.body.querySelector(
          'a[href]:not([disabled]):not(.tcu-visuallyhidden), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])'
        );

      // Let's get started!
      alertInit();

      if (toggleButton) {
          // Event Listeners
          toggleButton.addEventListener('click', toggleAlert);
          alertWrap.addEventListener('keydown', closeAlert);
      }

      function closeAlert(e) {
        var isEscapePressed = e.key === 'escape' || e.keyCode === 27;

        if (isEscapePressed && alertWrap.getAttribute('data-state') == 'open') {
          toggleAlert();
        }
      }

      // Function Declarations
      function toggleAlert() {
        if (alertWrap.getAttribute('data-state') != 'closed') {
            localState = 'closed';
            firstFocusableBody.focus();
        } else {
            localState = 'open';
        }
        alertWrap.setAttribute('data-state',localState);

        // Store event in localStorage
        localStorage.setItem('tcu_alert', localState);
      }

      /**
      * Traps the focus to within an element
      * @param  {Object} element the element that you want to trap focus in
      */
      function trapFocus(element) {
        var focusableEls = element.querySelectorAll(
          'a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])'
        );
        var firstFocusableEl = focusableEls[0];
        var lastFocusableEl = focusableEls[focusableEls.length - 1];
        var KEYCODE_TAB = 9;

        element.addEventListener('keydown', function (e) {
          var isTabPressed = e.key === 'Tab' || e.keyCode === KEYCODE_TAB;

          if (!isTabPressed) {
            return;
          }

          if (e.shiftKey) {
            /* shift + tab */
            if (document.activeElement === firstFocusableEl) {
              lastFocusableEl.focus();
              e.preventDefault();
            }
          } else {
            /* tab */
            if (document.activeElement === lastFocusableEl) {
              firstFocusableEl.focus();
              e.preventDefault();
            }
          }
        });
      }

      function alertInit() {

          if(alertWrap) {
              // if alert was closed
              if(localState == 'closed') {
                  alertWrap.setAttribute('data-state','closed');
              } else {
                  // Let's give our alert message an active state
                  // in case JS fails this message will still display
                  alertWrap.setAttribute('data-state','open');
                  alertLink.focus();
                  trapFocus(alertWrap);
              }
          }

      }
  })();
  </script>

	<?php
}
echo '<!-- end TCU Alerts -->';
