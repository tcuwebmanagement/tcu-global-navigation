
<link rel="apple-touch-icon" href="/_resources/images/apple-icon-touch.png">
<link rel="icon" href="/_resources/images/favicon.png">
<!--[if IE]>
<link rel="shortcut icon" href="/_resources/images/favicon.ico">
<![endif]-->
<meta name="msapplication-TileColor" content="#4d1979">
<meta name="msapplication-TileImage" content="/_resources/images/win8-tile-icon.png">
<script src="/_resources/js/libs/modernizr.custom.min.js"></script>
<script src="/_resources/js/libs/picturefill.min.js"></script>
<link rel="stylesheet" href="http://financialaid.dev.tcu.edu/_resources/css/style.min.css" type="text/css" media="all">
<link href="https://fonts.googleapis.com/css?family=Oswald%7CSource+Sans+Pro:400,700%7CPlayfair+Display:400,700" rel="stylesheet">

<?php include 'global-head.php'; ?>
