module.exports = {
  extends: ['eslint:recommended', 'prettier'], // extending recommended config and config derived from eslint-config-prettier
  plugins: ['prettier'], // activating eslint-plugin-prettier (--fix stuff)
  parserOptions: {
    ecmaVersion: 2017
  },
  rules: {
    'prettier/prettier': [
      // customizing prettier rules (unfortunately not many of them are customizable)
      'error',
      {
        singleQuote: true
      }
    ],
    eqeqeq: ['error', 'always'] // adding some custom ESLint rules
  },
  globals: {
    window: 'writable',
    document: 'writable',
    navigator: 'readonly',
    console: 'readonly'
  }
};
