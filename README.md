# tcu-global-navigation
This is the working project for the globally shared navigation files

- `_alerts.php_`
- `global-footer.php`
- `global-head.php`
- `global-nav.php`

---

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

In order to make the most of this project, you will need these tools:

- [VS Code](https://code.visualstudio.com/download)

- [Node Package Manager (NPM)](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

- [Gulp (requires NPM)](https://gulpjs.com/)

- [Laravel Valet (optional)](https://laravel.com/docs/5.8/valet#installation)

- _TCU Global Includes_

    1. Create a folder for global PHP shares and add it to your php.ini include_path value
    2. Copy or symlink `tcu-global-navigation/build/global-nav.php`, `_alerts.php_`, `global-footer.php`, and `global-head.php` to this location

### Installing

1. Open up a console and navigate to the root folder of the project.

2. Install the node modules required by the project.

    ```
    npm install
    ```

3. Navigate to the `site` folder and link the site for Valet (optional)

    ```
    cd site
    valet link tcu-global-navigation
    ```

4. Run Gulp. Once Gulp finishes running all of the tasks, the homepage should open automatically.

    ```
    gulp
    ```

## Development

Development is done in `_src` directory. Testing on localhost is done within the `site` directory.

The files that will be uploaded to the global share directory are in the `build` directory. These files are only built after running `gulp`.

### global-nav.php inline scripts

`_src/includes/global-nav.php` has a special comment for inlining css and javascript with a gulp task called "inline"

```
<style>
<!-- inject:../../site/_resources/css/style.min.css -->
<!-- endinject -->
</style>
```

This section will be replaced with the contents of `./site/_resources/css/style.min.css`.

You can run the build task manually with `gulp build` or it will run automatically when using browsersync through the default gulp task.

The final compiled version of `global-nav.php` will be in `build/global-nav.php`

### Hide Search
Sometimes you might need to hide the search icon or the entire search script on certain sections of a website. To do that,
add one of the following globals to your `headcode.php` file.

```
$GLOBALS["hide_search"] = true; // hides the search icon
$GLOBALS["hide_search_script"] = true; // hides the global search script
```
