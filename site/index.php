<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Texas Christian University | Financial Aid</title>
        <link rel="canonical" href="https://www.tcu.edu/index.php">

        <meta name="Description" content="Texas Christian University, a private university located in Fort Worth, Texas, was established in 1873 by brothers Addison and Randolph Clark.">

        <?php include '_resources/includes/headcode.php'; ?>

        <meta property="og:title" content="Texas Christian University">
        <meta property="og:url" content="https://www.tcu.edu/index.php">
        <meta property="og:description" content="Texas Christian University, a private university located in Fort Worth, Texas, was established in 1873 by brothers Addison and Randolph Clark.">

        <meta property="og:site_name" content="Texas Christian University">
        <meta property="og:type" content="">
        <meta property="og:updated_time" content="2019-02-21T11:13:01.411-08:00">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:url" content="https://www.tcu.edu/index.php">
        <meta name="twitter:title" content="Texas Christian University">

        <meta name="twitter:description" content="Texas Christian University, a private university located in Fort Worth, Texas, was established in 1873 by brothers Addison and Randolph Clark.">
        <meta name="twitter:creator" content="@TCU">
        <meta name="twitter:site" content="Texas Christian University">

        <script>
            var tcuglobal = {};
            tcuglobal.searchtab = "Financial Aid";
            tcuglobal.searchpath = "financialaid.tcu.edu";
            tcuglobal.searchfilter = "";
        </script>
    </head>
    <body>
        <?php include '_resources/includes/icons.php'; ?>
        <section class="tcu-hero -non-academic">
            <div class="tcu-hero__container">
                <picture>
                    <source media="(min-width: 640px)" srcset="https://placehold.it/1800x600?text=Hero%20Image">
                    <img src="https://placehold.it/640x480?text=Hero%20Image" alt="Placeholder Image" style="width:100%;object-fit:cover;">
                </picture>

                <!-- <div class="tcu-hero__content">
                    <h2 class="tcu-playfair">Scholarships &amp; Financial Aid
                    </h2>
                    <span>Nearly 150 years later, we are still that place.</span>
                </div> -->
            </div>
        </section>

        <div class="tcu-local-nav">
            <div class="js-nav-toggle" data-state>
                <a href="#">
                    <span>Menu</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="tcu-button-arrow" style="width: 30px; height: 30px;fill:#ffffff;" data-state><title>keyboard_arrow_down</title><desc>  Created with Sketch.</desc><g class="st0"><path d="M6.2 6.5l3.8 3.8 3.8-3.8L15 7.7l-5 5 -5-5L6.2 6.5z"/></g></svg></span>
                </a>
            </div>

            <div class="nav-wrapper">
                <nav class="tcu-main-navigation" role="navigation">
                    <div class="nav-header">
                        <span class="nav-back"></span>
                        <strong><span class="nav-title"></span></strong>
                        <span class="nav-close"></span>
                    </div>

                    <ul class="menu">
                        <li><a href="#">Home</a></li>
                        <li class="has-dropdown" data-state>
                            <a href="#">Apply for Aid</a>
                            <span id="1"></span>

                            <!-- Sub-Level 1 -->

                            <ul class="sub-menu sub1" data-menu>
                                <li class="sub-menu-header">Sub-Level-1</li>
                                <li><a href="#">Undergraduate</a></li>
                                <li><a href="#">Graduate/Professional</a></li>
                                <li><a href="#">Transfer</a></li>
                                <li><a href="#">International</a></li>
                                <li><a href="#">Veteran/Military</a></li>

                                <li class="has-dropdown" data-state>
                                    <a href="#">Special Degree/Certification</a><span id></span>

                                    <!-- Sub-Level 2 -->
                                    <ul class="sub-menu">
                                        <li class="sub-menu-header">
                                        &lt;Sub Level 2 Heading
                                        </li>
                                        <li><a href="#">Ranch Management</a></li>
                                        <li><a href="#">Teacher Certification</a></li>
                                        <li><a href="#">Accelerated Nursing</a></li>
                                        <li><a href="#">Theological Studies</a></li>

                                        <li class="has-dropdown" data-state>
                                            <a href="#">Sub-level 3</a><span id></span>
                                            <ul class="sub-menu">
                                                <li class="sub-menu-header">
                                                    <div>&lt;</div>Sub Level 3 Heading
                                                </li>
                                                <li><a href="#">Sub-level3-item</a></li>
                                                <li><a href="#">Sub-level3-item</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li><a href="#">Summer Aid</a></li>
                                <li><a href="#">Verification</a></li>
                            </ul>
                        </li>
                        <li class="has-dropdown" data-state>
                            <a href="#">Types of Aid</a><span id></span>
                                <ul class="sub-menu sub2" data-menu>
                                <li class="has-dropdown" data-state>
                                    <a href="#">Type1</a><span id></span>
                                    <ul class="sub-menu" data-menu>
                                        <li><a href="#">Item 1</a></li>
                                        <li><a href="#">Item 2</a></li>
                                        <li><a href="#">Item 3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Type 2</a></li>
                                <li><a href="#">Type 3</a></li>

                                <li class="has-dropdown" data-state>
                                    <a href="#">Another Layer</a><span id></span>
                                    <ul class="sub-menu">
                                        <li><a href="#">Item 1</a></li>
                                        <li><a href="#">Item 2</a></li>

                                        <li class="has-dropdown" data-state>
                                            <a href="#">Item 3-has dropdown</a><span id></span>
                                            <ul class="sub-menu" data-menu>
                                                <li><a href="#">Item 1</a></li>
                                                <li><a href="#">Item 2</a></li>
                                                <li><a href="#">Item 3</a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Cost</a>
                        </li>
                        <li class="has-dropdown" data-state>
                            <a href="#">Resources</a><span id></span>
                            <ul class="sub-menu" data-menu>
                                <li class="has-dropdown">
                                    <a href="#">Resource A</a><span id></span>
                                    <ul class="sub-menu" data-menu>
                                        <li><a href="#">resources a.1</a></li>
                                        <li><a href="#">resources a.2</a></li>
                                        <li><a href="#">resources a.3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Resource B</a></li>
                                <li><a href="#">Resource C</a></li>
                            </ul>
                        </li>
                        <li class="has-dropdown" data-state>
                            <a href="#" class="tcu-nav-link">Consumer Information</a><span id></span>
                            <ul class="sub-menu" data-menu>
                                <li><a href="#">consumer info.1</a></li>
                                <li><a href="#">consumer info.2</a></li>
                                <li><a href="#">consumer info.3</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="tcu-nav-link">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div><!-- / .nav-wrapper -->
        </div><!-- / .tcu-local-nav -->

        <div class="tcu-layout-container">
            <main class="size1of1 m-size1of1 tcu-main tcu-layout-constrain cf">
                <a name="main" tabindex="-1" id="main"><span class="tcu-visuallyhidden">Main Content</span></a>
                <h1>TCU Privacy of Information Policy</h1>
                <p>Information stored on a computer system or sent electronically over a network is the property of the individual who created it. Examination of that information without authorization from the owner is a violation of the owner’s rights to control his or her own property. Systems administrators, however, may gain access to user’s files when it is necessary to maintain or prevent damage to systems or to ensure compliance with other University rules.</p>
                <p>Computer systems and networks provide mechanisms for the protection of private information from examination. These mechanisms are necessarily imperfect and any attempt to circumvent them in order to gain unauthorized access to private information (including both stored computer files and messages transmitted over network) will be treated as a violation of privacy and may subject a violator to disciplinary action.</p>
                <p>In general, information that the owner would reasonably regard as private must be treated as private by other users. Examples include the contents of electronic mail boxes, the private file storage areas of individual users, and information stored in other areas that are not public. That measures have not been taken to protect such information does not make it permissible for others to inspect it.</p>
                <p>On shared and networked computer systems certain information about users and their activities is visible to others. Users are cautioned that certain accounting and directory information (for example, user names and electronic mail addresses), certain records of file names and executed commands, and information stored in public areas, are not private. Nonetheless, such unsecured information about other users must not be manipulated in ways that they might reasonably find intrusive; for example, eavesdropping by computer and systematic monitoring of the behavior of others are likely to be considered invasions of privacy that would be cause for disciplinary action.</p>
            </main>
            <?php include '_resources/includes/footer.php'; ?>
        </div><!-- / .tcu-layout-container -->


        <?php include 'global-nav.php'; ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    </body>
</html>
