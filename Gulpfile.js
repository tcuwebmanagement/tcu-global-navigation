/* eslint-disable no-undef */
/**
 * Require our dependencies
 */
const { dest, lastRun, parallel, series, src, watch } = require('gulp');
const autoprefixer = require('autoprefixer');
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const eslint = require('gulp-eslint');
const gulpStylelint = require('gulp-stylelint');
const imagemin = require('gulp-imagemin');
const inject = require('gulp-inject');
const nano = require('cssnano');
const postcss = require('gulp-postcss');
const purgecss = require('@fullhuman/postcss-purgecss');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const spritesmith = require('gulp.spritesmith');
const svgstore = require('gulp-svgstore');
const touch = require('gulp-touch-cmd');
const uglify = require('gulp-uglify');

const siteName = 'tcu-global-navigation'; // set your siteName here

/**
 * Set assets paths.
 */
const paths = {
  build: 'build',
  buildFiles: [
    './site/_resources/includes/_alerts.php',
    './site/_resources/includes/global-footer.php',
    './site/_resources/includes/global-head.php'
  ],
  compiledjs: '_src/js/compiled/*.js',
  css: ['_src/css/*.css', '!_src/css/*.min.css'],
  icons: '_src/images/svg/icon-library/*.svg',
  images: [
    '_src/images/**',
    '!_src/images/svg/icon-library{,/**}',
    '!_src/images/sprites{,/**}'
  ],
  php: ['site/*.php', 'site/**/*.php'],
  phpincludes: ['_src/includes/*.php'],
  resources: 'site/_resources',
  sass: ['_src/scss/**/*.scss', '_cornerstone/components/**/*.scss'],
  scripts: ['_src/js/**.js', '!_src/js/*.min.js'],
  sprites: '_src/images/sprites/*.png'
};

/**
 * Shared options
 */
const imageminOptions = {
  optimizationLevel: 5,
  progressive: true,
  interlaced: true,
  svgoPlugins: [
    {
      removeViewBox: false,
      removeComments: true,
      removeAttrs: { attrs: ['xmlns'] }
    }
  ]
};

function watchFiles() {
  watch(paths.php).on('change', browserSync.reload);
  watch(paths.icons, icons);
  watch(paths.images, images);
  watch(paths.phpincludes, series(includesFiles, buildFiles, inline)).on(
    'change',
    browserSync.reload
  );
  watch(paths.sass, series(styles, inline)).on('change', browserSync.reload);
  watch(paths.scripts, series(scripts, inline)).on(
    'change',
    browserSync.reload
  );
  watch(paths.sprites, sprites);
}

/**
 * Browser sync init
 */
function browser_sync(cb) {
  browserSync.init({
    open: 'external', // Open project in a new tab?
    proxy: 'http://' + siteName + '.test', // Use your localhost path to use BrowserSync - Note: using Valet here.
    host: siteName + '.test'
  });
  // Watch
  watchFiles();

  cb();
}

/**
 * Optimizing and compiling icon library svgs into symbols
 */
function icons(cb) {
  src(paths.icons)
    .pipe(imagemin(imageminOptions))
    .pipe(rename({ prefix: 'icon-' }))
    .pipe(svgstore({ inlineSvg: true }))
    .pipe(dest(`${paths.resources}/images/svg`));

  cb();
}

/**
 * Images
 */
function images(cb) {
  src(paths.images, { since: lastRun(images) })
    .pipe(imagemin(imageminOptions))
    .pipe(dest(`${paths.resources}/images/`));

  cb();
}

/**
 * Sync working folder of PHP includes with Site folder
 *
 * Removes existing includes for a fresh start
 *
 */
function includesFiles() {
  return src(['./_src/includes/*.php'], { base: './_src/includes/' })
    .pipe(dest(`${paths.resources}/includes`))
    .pipe(browserSync.stream());
}

function buildFiles() {
  return src(paths.buildFiles, {
    base: './site/_resources/includes/'
  })
    .pipe(dest(paths.build))
    .pipe(browserSync.stream());
}

function inline() {
  // const target = src('./site/_resources/includes/global-nav.php');
  const sources = src([
    './site/_resources/js/**/*.js',
    './site/_resources/css/**/*.css'
  ]);

  return src([
    './_src/includes/global-nav.php',
    './_src/includes/global-footer.php'
  ])
    .pipe(
      inject(sources, {
        starttag: '<!-- inject:{{path}} -->',
        relative: true,
        removeTags: true,
        transform: function(filePath, file) {
          // return file contents as string
          return file.contents.toString('utf8');
        }
      })
    )
    .pipe(dest('./build'))
    .pipe(touch())
    .pipe(browserSync.stream());
}

/**
 * Lint, compile, and optimize our scripts
 */
function scripts() {
  return src(paths.scripts, { since: lastRun(scripts) })
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(babel())
    .pipe(uglify({ mangle: false }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest(`${paths.resources}/js/min`))
    .pipe(browserSync.stream());
}

function sprites(cb) {
  // Generate our spritesheet
  var spriteData = src(paths.sprites)
    .pipe(imagemin(imageminOptions))
    .pipe(
      spritesmith({
        imgName: 'sprites.png',
        cssName: '_sprites.scss',
        algorithm: 'binary-tree'
      })
    );

  // Pipe image stream through image optimizer and onto disk
  spriteData.img
    .pipe(dest(`${paths.resources}/images/`))
    .pipe(browserSync.stream());

  // Pipe CSS stream through CSS optimizer and onto disk
  spriteData.css.pipe(dest(`_src/scss/globals`)).pipe(browserSync.stream());

  cb();
}

/**
 * Lint, compile, and optimize style.css
 */
function styles() {
  // Settings
  const sassOptions = { errLogToConsole: true };
  const postCSSPlugins = [
    autoprefixer,
    purgecss({
      content: [
        './_src/includes/global-nav.php',
        './_src/js/tcu-global-navigation.js',
        './_src/includes/global-footer.php',
        './_src/includes/footer.php'
      ],
      whitelistPatternsChildren: [
        /tcu-nav-wrap/,
        /--open/,
        /body/
      ]
    }),
    nano
  ];

  return src('_src/scss/*.scss')
    .pipe(
      gulpStylelint({
        reporters: [{ formatter: 'string', console: true }]
      })
    )
    .pipe(sass.sync(sassOptions).on('error', sass.logError))
    .pipe(postcss(postCSSPlugins))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(dest(`${paths.resources}/css/`))
    .pipe(browserSync.stream());
}

exports.icons = icons;
exports.scripts = scripts;
exports.sprites = sprites;
exports.styles = styles;
exports.build = series(
  parallel(icons, images, scripts, sprites, styles, includesFiles),
  buildFiles,
  inline
);
exports.default = series(
  parallel(scripts, styles, includesFiles),
  buildFiles,
  inline,
  browser_sync
);
